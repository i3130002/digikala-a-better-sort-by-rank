# Digikala.com product extractor

Ever wanted to extract the list of product based of their actual rating?
Saw this video(Cannot find it. Might be removed. Use [this article](https://stackoverflow.com/questions/29991010/how-to-compare-two-products-based-on-their-ratings) ) about the rating and how to compare two different ratings given different total votes.

This code will fetch the products from digikala.com given a search URL `baseURL` and outputs to the output.html

For test purposes I attached mens jeans in ex.output.html  and the output is something like below image:

![image-20211005183754315](readme.assets/image-20211005183754315.png)


# Updates:

## V1.1:

* Support for manual title filtering
* Code refactor
* Manual max fetch page
* Manual URL
* Normal URL support (instead of AJAX)
* Add link to digikala listing
* Add link to this repo
* User input filter words

# License

It is Free to use. As is with no responsibilities. Using it in your product or future development needs to have a reference to this repository.
