import requests
pageNumberFlag = "PAGENUMBER"
finalList = []
baseURL = ''
maxPages = 2
HavingWordsInTitle = []


def getDigikalaURL():
    print("Sample page URL: https://www.digikala.com/search/category-men-jeans/?has_selling_stock=1&pageno=1&sortby=4\n")
    digikalaSearchURL = input("Give the full page url:")
    if "" not in digikalaSearchURL:
        print("Incompatible URL")
        return getDigikalaURL()
    return digikalaSearchURL

def intTryParse(value,default):
    try:
        return int(value)
    except ValueError:
        return default

def prepareBaseURL(listingURL):
  baseURL = listingURL.replace(
    "digikala.com/search", "digikala.com/ajax/search")
  pageNumberFilter = "pageno="
  return baseURL[:baseURL.index(pageNumberFilter)+len(pageNumberFilter)] + \
      pageNumberFlag + baseURL[baseURL.index(pageNumberFilter)+len(pageNumberFilter)+1:]

def getMaxPages():
  maxPages = intTryParse(input("Maximum pages to fetch(empty to fetch all):"),None)
  if maxPages is None:
    firstPage = requests.get(baseURL.replace(pageNumberFlag, "1"))
    responseObj = firstPage.json()
    return int(responseObj["data"]["trackerData"]["pages"])
  return maxPages

def appendToList(responseObj):
    click_impressionList = responseObj["data"]["click_impression"]
    for click_impression in click_impressionList:
        rate = float(click_impression["rating"]["rate"])
        count = float(click_impression["rating"]["count"])
        click_impression["rating"]["project"] = rate * count / (count+1)
        finalList.append(click_impression)

def appendAllProducts():
  for page in range(1, maxPages+1):
      firstPage = requests.get(baseURL.replace(pageNumberFlag, str(page)))
      responseObj = firstPage.json()
      appendToList(responseObj)
      print(f"Processing {page} of {maxPages}")


def productSorter(e):
    return e["rating"]["project"]

def filterTitleContainWords(wordList):
  for product in finalList:
    for word in wordList:
      if word not in product["name"]:
        product["doNotDisplay"]=True
        break
  
def generateOutput(HavingWordsInTitle):
  f = open("output.html", "w")
  f.write(f""" <!DOCTYPE html>
  <html>
  <head>
  <title>Digikala.com top products</title>
  <meta charset="UTF-8">
  </head>
  <body>
  <h1><a href="{listingURL}">لینک به لیستینگ دیجیکالا</a></h1><br>
  <p>عنوان شامل تمام کلمات: {HavingWordsInTitle}<p><br>
  <h1><a href="https://gitlab.com/i3130002/digikala-a-better-sort-by-rank">تولید شده توسط</a></h1><br><br><br>
  """)

  for product in finalList:
      if "doNotDisplay" in product and product["doNotDisplay"] == True:
        continue
      url = product["product_url"]
      imageURL = product["image_src"]
      name = product["name"]
      brand = product["brand"]
      price = product["price_detail"]["selling_price"]/10000
      rate = product["rating"]["rate"]
      count = product["rating"]["count"]
      project = product["rating"]["project"]

      f.write(f"""
  <img src="{imageURL}" style="width: 300px;"  loading="lazy">
  <h2><a href="{url}">{name} - {brand}</a></h2>
  <p>هزار تومان:{price}</p>
  <p>برآورد امتیاز:{project} (امتیاز:{rate}/تعداد:{count})</p>
  <br><br>
  """)
  f.write("""</body>
  </html> """)
  f.close()

def getHavingWordsFromUser():
  print("Give words that should exists in product title(press enter to end):")
  while True:
    word = input("Word:")
    if word == '':
      break
    HavingWordsInTitle.append(word)

if __name__ == "__main__":
  listingURL = getDigikalaURL()
  baseURL = prepareBaseURL(listingURL)
  maxPages = getMaxPages()
  getHavingWordsFromUser()
  appendAllProducts()
  filterTitleContainWords(HavingWordsInTitle)
  finalList.sort(key=productSorter, reverse=True)
  generateOutput(HavingWordsInTitle)
